var gulp = require('gulp');
var browserSync = require('browser-sync').create();

module.exports = browserSync;

gulp.task('browser-sync', ['styles', 'watch'], function (callback) {
  browserSync.init({
    server: {
      baseDir: ['src', '.tmp']
    },
    notify: false
  }, callback)
});
