var gulp = require('gulp');
var watch = require('gulp-watch');

var browserSync = require('./browser-sync');

gulp.task('watch', function () {
  watch([
    'src/styles/**/*.scss'
  ], function () {
    gulp.start('styles');
  });

  watch([
    'src/*.html',
    'src/images/**/*',
    'src/scripts/**/*'
  ], browserSync.reload)
});
