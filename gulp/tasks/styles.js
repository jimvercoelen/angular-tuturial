var gulp = require('gulp');
var sass = require('gulp-sass');

var browserSync = require('./browser-sync');
var common = require('../common');

gulp.task('styles', function () {
  return common.getCombinerPipe('styles', [
    gulp.src([
      'src/styles/main.scss'
    ]),
    sass(),
    gulp.dest('.tmp/styles'),
    browserSync.stream()
  ]);
});
